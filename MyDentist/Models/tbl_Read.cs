//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDentist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Read
    {
        public int ID_Read { get; set; }
        public string Reader { get; set; }
        public Nullable<System.DateTime> Date_read { get; set; }
        public Nullable<int> ID_Article { get; set; }
    
        public virtual tbl_Article tbl_Article { get; set; }
    }
}
