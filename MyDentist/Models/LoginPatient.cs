﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDentist.Models
{
    public class LoginPatient
    {
        [Display(Name ="Username")]
        [Required(AllowEmptyStrings =false,ErrorMessage ="Please Enter your email !")]
        public string Username { get; set; }

        [Display(Name ="Password")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter your Password !")]
        public string Password { get; set; }

        [Display(Name ="Remember Me")]
        public bool RememberMe { get; set; }
    }
}