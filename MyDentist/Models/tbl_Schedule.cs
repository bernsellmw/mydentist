//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyDentist.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Schedule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_Schedule()
        {
            this.tbl_Reservation = new HashSet<tbl_Reservation>();
        }
    
        public int ID_Schedule { get; set; }
        public Nullable<System.TimeSpan> Start_hour { get; set; }
        public Nullable<System.TimeSpan> End_hour { get; set; }
        public Nullable<int> ID_Dentist { get; set; }
        public Nullable<int> ID_Clinic { get; set; }
    
        public virtual tbl_Clinic tbl_Clinic { get; set; }
        public virtual tbl_Dentist tbl_Dentist { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_Reservation> tbl_Reservation { get; set; }
    }
}
