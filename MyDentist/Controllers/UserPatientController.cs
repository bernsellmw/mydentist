﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MyDentist.Models;
using Microsoft.AspNet.Identity;

namespace MyDentist.Controllers
{
    public class UserPatientController : Controller
    {
        // GET: UserPatient
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude ="IsEmailVerified,ActivationCode")]tbl_Patient UserPatient)
        {
            try
            {
                bool status = false;
                string message = "";
                if (ModelState.IsValid)
                {
                    #region
                    var isExist = IsEmailExist(UserPatient.Email);
                    if (isExist)
                    {
                        ModelState.AddModelError("Email exist", "Email already Exist");
                        return View(UserPatient);
                    }
                    #endregion

                    #region Generate Activation Code
                    UserPatient.ActivationCode = Guid.NewGuid();
                    #endregion


                    #region Password Hashing
                    UserPatient.Password = Crypto.Hash(UserPatient.Password);
                    UserPatient.ConfirmPassword = Crypto.Hash(UserPatient.ConfirmPassword);
                    #endregion
                    UserPatient.IsEmailVerified = false;

                    #region Save to Database
                    using (db_mydentistEntities1 db = new db_mydentistEntities1())
                    {
                        db.tbl_Patient.Add(UserPatient);
                        db.SaveChanges();

                        //Send Email to user
                        SendVerificationLinkEmail(UserPatient.Email, UserPatient.ActivationCode.ToString());
                        message = "Registration successfully done. Account activation link " +
                    " has been sent to your email id:" + UserPatient.Email;

                        status = true;
                    }
                    #endregion
                }
                else
                {
                    message = "Invalid Request";
                }
                ViewBag.Message = message;
                ViewBag.Status = status;
                return View(UserPatient);
            }
            catch (DbEntityValidationException e)
            {
                string errmessage = "";
                foreach (var eve in e.EntityValidationErrors)
                {
                    errmessage+=("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    errmessage += "\n";
                    foreach (var ve in eve.ValidationErrors)
                    {
                        errmessage+=("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        errmessage += "\n";
                    }
                }
                ViewBag.Message = errmessage;
                return View();
                //throw;
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                var v = db.tbl_Patient.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v!=null)
                {
                    v.IsEmailVerified = true;
                    db.SaveChanges();
                    status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginPatient login, string ReturnUrl)
        {
            string message = "";
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Patient.Where(a => a.Username == login.Username).FirstOrDefault();
                if(v!=null)
                {
                    if(string.Compare(Crypto.Hash(login.Password),v.Password)==0)
                    {
                        int timeout = login.RememberMe ? 52600 : 20; //1 year
                        var ticket = new FormsAuthenticationTicket(login.Username, login.RememberMe, timeout);
                        string encrypted = FormsAuthentication.Encrypt(ticket);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);

                        if(Url.IsLocalUrl(ReturnUrl))
                        {
                            return Redirect(ReturnUrl);
                        }
                        else
                        {
                            return RedirectToActionPermanent("LoggedIndex", "Home");
                        }

                    }
                    else
                    {
                        message = "Wrong Password";
                    }

                }
                else
                {
                    message = "EUsername doesn't Exist";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "UserPatient");
        }



        [NonAction]
        public bool IsEmailExist(string emailPatient)
        {
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var v = db.tbl_Patient.Where(a => a.Email == emailPatient).FirstOrDefault();
                return v != null;
            }
        }



        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode,string emailFor="VerifyAccount")
        {
            var verifyurl = "/UserPatient/"+emailFor+"/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyurl);

            var fromEmail = new MailAddress("b.ansell.m.w@gmail.com", "My Dentist");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "salamander00";

            string subject = "";
            string body = "";
            if(emailFor=="VerifyAccount")
            {
                subject = "Your account is succesfully created";

                body = "<br/><br/>We are excited to tell you that your MyDentist account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a> ";
            }
            else if (emailFor=="ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi,<br/>br/>We got request for reset your account password. Please click on the below link to reset your password" +
                "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }

            
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };
            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            }) 
                smtp.Send(message);
        }

        //Forgot Password

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgotPassword(string Email)
        {
            string message = "";
            bool status = false;
            using (db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var account = db.tbl_Patient.Where(a => a.Email == Email).FirstOrDefault();
                if(account!=null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email,resetCode,"ResetPassword");
                    account.ResetPassword = resetCode;
                    //To avoid confirm password didnt match
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.SaveChanges();
                    message = "Reset Password has been sent to your email";
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            using(db_mydentistEntities1 db = new db_mydentistEntities1())
            {
                var user = db.tbl_Patient.Where(a => a.ResetPassword == id).FirstOrDefault();
                if(user != null)
                {
                    ResetPasswordPatient model = new ResetPasswordPatient();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordPatient model)
        {
            var message = "";
            if(ModelState.IsValid)
            {
                using (db_mydentistEntities1 db = new db_mydentistEntities1())
                {
                    var user = db.tbl_Patient.Where(a => a.ResetPassword == model.ResetCode).FirstOrDefault();
                    if(user!=null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPassword = "";

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        message = "Password Updated succesfully";
                    }
                }
            }
            else
            {
                message = "Something Invalid";
            }

            ViewBag.Message = message;
            return View(model);
        }
       
    }

}