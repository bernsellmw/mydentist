﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using MyDentist.Models;

namespace MyDentist.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        [Authorize]
        public ActionResult LoggedIndex()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Gmap()
        {
            
            return View();
        }
        [Authorize]
        [HttpPost]
        public ActionResult Gmap()
        {
            string uname = HttpContext.User.Identity.Name;
            ViewBag.Message = uname;
            return View();
        }

    }
}